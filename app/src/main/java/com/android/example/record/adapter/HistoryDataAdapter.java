package com.android.example.record.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.example.record.R;
import com.android.example.record.activity.DetailsActivity;
import com.android.example.record.bean.HistoryData;
import com.android.example.record.databinding.HistoryDataItemBinding;
import com.android.example.record.utils.Utils;
import com.bumptech.glide.Glide;

import java.util.List;

public class HistoryDataAdapter extends RecyclerView.Adapter<HistoryDataAdapter.ViewHolder> implements View.OnClickListener {

    private final List<HistoryData> historyDataList;
    private Context context;

    private String img_path;

    private OnItemClickListener onItemClickListener;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final HistoryDataItemBinding binding;

        public ViewHolder(@NonNull HistoryDataItemBinding itemBinding) {
            super(itemBinding.getRoot());
            binding = itemBinding;

            binding.getRoot().setOnClickListener(HistoryDataAdapter.this);
            binding.options.setOnClickListener(HistoryDataAdapter.this);
        }
    }

    public HistoryDataAdapter(List<HistoryData> historyDataList) {
        this.historyDataList = historyDataList;
    }

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        if (onItemClickListener != null) {
            if (v.getId() == R.id.sub_recycler_view) {
                onItemClickListener.onItemClick(v, ViewName.PRACTISE, position);
            } else {
                onItemClickListener.onItemClick(v, ViewName.ITEM, position);
            }
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (context == null) {
            context = parent.getContext();
        }

        img_path = Utils.getImgPath(context);
        HistoryDataItemBinding binding = HistoryDataItemBinding.inflate(LayoutInflater.from(context), parent, false);
        ViewHolder holder = new ViewHolder(binding);

        holder.binding.getRoot().setOnClickListener(v -> {
            int position = holder.getBindingAdapterPosition();
            HistoryData historyData = historyDataList.get(position);
            Intent intent = new Intent(context, DetailsActivity.class);
            intent.putExtra("ID", historyData.getId());
            context.startActivity(intent);
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        img_path = Utils.getImgPath(context);

        HistoryData historyData = historyDataList.get(position);
        holder.binding.saveTime.setText(String.format("%s %s %s", historyData.getSave_date(), historyData.getSave_week(), historyData.getSave_time()));
        holder.binding.siren.setText(historyData.getSiren());
        holder.binding.number.setText(historyData.getNumber());
        holder.binding.color.setText(historyData.getColor());
        holder.binding.address.setText(historyData.getAddress());
        Glide.with(context).load(img_path + historyData.getImg_front_uri()).into(holder.binding.imgFront);
        Glide.with(context).load(img_path + historyData.getImg_overall_uri()).into(holder.binding.imgOverall);
        Glide.with(context).load(img_path + historyData.getImg_rear_uri()).into(holder.binding.imgRear);
        Glide.with(context).load(img_path + historyData.getImg_inside_uri()).into(holder.binding.imgInside);
        holder.binding.getRoot().setTag(position);
        holder.binding.options.setTag(position);
    }

    @Override
    public int getItemCount() {
        if (historyDataList != null) {
            return historyDataList.size();
        }
        return 0;
    }

    public enum ViewName {
        ITEM,
        PRACTISE
    }

    public interface OnItemClickListener {
        void onItemClick(View view, ViewName viewName, int position);

        void onItemLongClick(View view);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }
}
