package com.android.example.record.viewmodel;

import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class RecordIDViewModel extends ViewModel {
    public MutableLiveData<Integer> record_id;

    public MutableLiveData<Integer> getRecordID() {
        if (record_id == null) {
            record_id = new MediatorLiveData<>();
        }

        return record_id;
    }
}
