package com.android.example.record.activity;

import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.example.record.adapter.RecordAdapter;
import com.android.example.record.bean.RecordBean;
import com.android.example.record.databinding.ActivityQueryRecordBinding;
import com.android.example.record.utils.Utils;

import org.litepal.LitePal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QueryRecordActivity extends AppCompatActivity {

    private ActivityQueryRecordBinding binding;

    private final List<RecordBean> subRecordBeanList = new ArrayList<>();
    private RecordAdapter recordAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = ActivityQueryRecordBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        initList(" ");
        initView();

        binding.hide.setOnClickListener(this::finish);

        binding.queryRecord.setOnClickListener(this::queryRecord);
    }

    private void queryRecord(View view) {
        String year = String.format("%4s", binding.yearText.getText().toString()).replaceAll("\\s", "0");
        String month = String.format("%2s", binding.monthText.getText().toString()).replaceAll("\\s", "0");
        String day = String.format("%2s", binding.dayText.getText().toString()).replaceAll("\\s", "0");

        String date;
        if (!year.equals("0000")) {
            date = year;
            if (!month.equals("00")) {
                date += month;
                if (!day.equals("00")) {
                    date += day;
                }
            }
        } else {
            date = "";
        }

        refreshList(date);
        Toast toast;
        if (subRecordBeanList.size() > 0) {
            toast = Toast.makeText(QueryRecordActivity.this, "查询成功", Toast.LENGTH_SHORT);
        } else {
            toast = Toast.makeText(QueryRecordActivity.this, "查询无结果", Toast.LENGTH_SHORT);
        }
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private void refreshList(String date) {
        runOnUiThread(() -> {
            recordAdapter.notifyItemRangeRemoved(0, subRecordBeanList.size());
            binding.subRecyclerView.removeAllViews();
            subRecordBeanList.clear();
            initList(date);
            recordAdapter.notifyItemRangeInserted(0, subRecordBeanList.size());
            Utils.closeKeyboard(QueryRecordActivity.this);
        });
    }

    private List<RecordBean> getRecordBeanList(String date) {
        List<RecordBean> mRecordBeanList = Collections.emptyList();
        if (TextUtils.isEmpty(date)) {
            return mRecordBeanList;
        }

        mRecordBeanList = LitePal.select("*").where("INSTR (record_name, ?) = 1", date).find(RecordBean.class);

        return mRecordBeanList;
    }

    private void initList(String date) {
        subRecordBeanList.addAll(getRecordBeanList(date));
        recordAdapter = new RecordAdapter(subRecordBeanList, this);
    }

    private void initView() {

        binding.yearText.setRawInputType(InputType.TYPE_CLASS_NUMBER);
        binding.monthText.setRawInputType(InputType.TYPE_CLASS_NUMBER);
        binding.dayText.setRawInputType(InputType.TYPE_CLASS_NUMBER);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.subRecyclerView.setLayoutManager(layoutManager);
        binding.subRecyclerView.setAdapter(recordAdapter);
    }

    public void hideSelf() {
        binding.layout.setVisibility(View.INVISIBLE);
        binding.playRecordFragmentView.setVisibility(View.VISIBLE);
    }

    public void showSelf() {
        binding.layout.setVisibility(View.VISIBLE);
        System.out.println("点击完成");
    }

    public void hideFragmentView() {
        binding.playRecordFragmentView.setVisibility(View.GONE);
    }

    private void finish(View view) {
        Utils.closeKeyboard(QueryRecordActivity.this);
        finish();
    }
}